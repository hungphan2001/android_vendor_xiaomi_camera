LEICA CAMERA FOR ALIOTH/ALIOTHIN

To use this camera first of all clone the repo:

        git clone https://gitlab.com/Eidoron1/android_vendor_xiaomi_camera.git -b miui13 vendor/xiaomi/camera

You need the following device change:

        https://github.com/SuperiorOS-Devices/device_xiaomi_alioth/commit/e6e37ed8a4f97f96afba4bc9113632928e75b59a

For common tree add this:

        https://github.com/SuperiorOS-Devices/device_xiaomi_sm8250-common/commit/e3410854a15a6277f8f0e80d68a130c6695610cb

To tone down camx logspams

	https://github.com/SuperiorOS-Devices/device_xiaomi_sm8250-common/commit/102b45a48c4f64244280236a9e38662ea4fc67de

Now some sepolicies for mi camera:

        https://github.com/SuperiorOS-Devices/device_xiaomi_sm8250-common/commit/2f312b3c74ba3cbbf35966905cddba40e461eb90

        https://github.com/SuperiorOS-Devices/device_xiaomi_sm8250-common/commit/f7bf096fd499aec0c9e2715f1fe2b9a6a1b5b3b7

And thats it. Now start building you rom normally.
